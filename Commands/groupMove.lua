function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move a group of units",
        parameterDefs = {
            {
                name = "unitsGroup",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }, 
            {
                name = "targetPoint",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }, 
            {
                name = "spread",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "128",
            },
            {
                name = "moveViaAttack",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "true",
            },
            {
                name = "targetPointThreshold",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "false",
            },
        }
    }
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringGetUnitCommands = Spring.GetUnitCommands
local MathRandom = math.random


function Run(self, units, parameter)

    local unitsGroup = parameter.unitsGroup

    if unitsGroup == nil then
        return SUCCESS
    end

    local targetPoint = parameter.targetPoint
    local spread = parameter.spread * #unitsGroup
    local targetPointThreshold = parameter.targetPointThreshold or (spread * 3)

    local cmd = CMD.MOVE
    
    if parameter.moveViaAttack == true then
        cmd = CMD.FIGHT
    end

    if self.commandsIssued == nil then
        self.commandsIssued = {}
        self.currentTargets = {}
    end
    

    -- issue orders
    local issuedNewCommand = false
    for i = 1, #unitsGroup do
        local unitId = unitsGroup[i]
 
        if self.commandsIssued[unitId] ~= true then 
            local pointX, pointY, pointZ = SpringGetUnitPosition(unitId)
            local unitPosition = Vec3(pointX, pointY, pointZ)
            if unitPosition:Distance(targetPoint) > targetPointThreshold then
                local spreadX = MathRandom(spread) - spread / 2
                local spreadZ = MathRandom(spread) - spread / 2
                SpringGiveOrderToUnit(unitId, cmd, (targetPoint + Vec3(spreadX, 0, spreadZ)):AsSpringVector(), {})    
                issuedNewCommand = true
            end

            self.currentTargets[unitId] = targetPoint
            self.commandsIssued[unitId] = true
        else
            if (Sensors.getUnitName(unitId) == "armmav") then
                --Spring.Echo("Current target is: " .. self.currentTargets[unitId].x .. " " .. self.currentTargets[unitId].y .. " " .. self.currentTargets[unitId].z)
            end
        end
    end
        
    if issuedNewCommand then
        return RUNNING
    end
    

    -- Keep running while the units are not close to the target point
	for i=1, #unitsGroup do
		local unitId = unitsGroup[i]
		local pointX, pointY, pointZ = SpringGetUnitPosition(unitId)
		local unitPosition = Vec3(pointX, pointY, pointZ)

		if unitPosition:Distance(targetPoint) > targetPointThreshold then
            if SpringGetUnitHealth(unitId) ~= nil and #SpringGetUnitCommands(unitId) > 0 then
				return RUNNING
            end
        else
            self.currentTargets[unitId] = nil
		end
	
	end

    self.commandsIssued = nil
    -- else success
    return SUCCESS
end

function Reset(self)
    self.commandsIssued = nil
    self.currentTargets = nil
end