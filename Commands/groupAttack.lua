function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Do a group attack",
        parameterDefs = {
            { 
                name = "group",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "target",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitCommands = Spring.GetUnitCommands

local modifier = {}

-- Attacks a given target with a given group
function Run(self, units, parameter)

    local group = parameter.group
    local target = parameter.target

    if group == nil then
        return SUCCESS
    end

    if self.givenOrders == nil then
        self.givenOrders = {}
    end

    local isRunning = false

    for i=1, #group do
        local unitId = group[i]
        if self.givenOrders[unitId] ~= true then
            local targetType = type(target)
            if targetType == "number" then
                -- Expect that the target is id of an enemy
                SpringGiveOrderToUnit(unitId, CMD.ATTACK, {target}, modifier)
            elseif targetType == "table" then
                -- Expect that the target is a position
                SpringGiveOrderToUnit(unitId, CMD.ATTACK, target:AsSpringVector(), modifier)
            else
                return FAILURE
            end
            
            self.givenOrders[unitId] = true
            isRunning = true
        else
            local numOfCommands = #SpringGetUnitCommands(unitId)
            isRunning = isRunning or (numOfCommands > 0 and Sensors.isUnitAlive(unitId))
        end
    end

    if isRunning == true then
        return RUNNING
    end

    return SUCCESS
end


function Reset(self)
    self.givenOrders = nil
end