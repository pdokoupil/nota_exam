function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Draw short lines at the given points",
        parameterDefs = {
            { 
                name = "points",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "linesBaseId",
                variableType = "number",
                componentType = "editBox",
                defaultValue = "1",
            },
            { 
                name = "width",
                variableType = "number",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "color",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "Vec3(1,0,0)",
            },
            { 
                name = "alpha",
                variableType = "number",
                componentType = "editBox",
                defaultValue = "1",
            },
        }
    }
end


-- Requires exampleDebug_update function for the drawing itself. 
function Run(self, units, parameter)
    
    local baseId = parameter.linesBaseId

    local lineWidth = parameter.width
    local color = parameter.color
    local alpha = parameter.alpha

    for id, loc in pairs(parameter.points) do
        
        local lineData = {
            startPos = loc - Vec3(5, 0, 0), 
            endPos = loc + Vec3(5, 0, 0)
        }

        local drawingOptions = {
            color = color, -- RGB color
            lineWidth = lineWidth,
            alpha = alpha
        }

        if (Script.LuaUI('exampleDebug_update')) then
            Script.LuaUI.exampleDebug_update(
                baseId+id,
                lineData,
                drawingOptions
            )
        end
    end
    return SUCCESS
end


function Reset(self)
    return self
end