function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Gives area based order to the unit .. i.e. reclaim for farks or repair for farks",
        parameterDefs = {
            { 
                name = "unitId",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "radius",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "command",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "CMD.RECLAIM",
            },
            {
                name = "modifier",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "nil",
            }
        }
    }
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitCommands = Spring.GetUnitCommands

function Run(self, units, parameter)
    
    local unitId = parameter.unitId
    local position = parameter.position
    local radius = parameter.radius
    local command = parameter.command
    local modifier = parameter.modifier

    if modifier == nil then
        modifier = {}
    else
        modifier = {modifier}
    end

    if self.commandInProgress == nil then
        self.commandInProgress = {}
    end

    if self.commandInProgress[unitId] == nil then
        SpringGiveOrderToUnit(unitId, command, {position.x, position.y, position.z, radius}, modifier)
        self.commandInProgress[unitId] = true
        return RUNNING
    else
        -- The command was already enqueued
        local commandQueue = SpringGetUnitCommands(unitId)
        if commandQueue ~= nil and #commandQueue > 0 then
            return RUNNING -- the command is still running
        end
    end

    return SUCCESS
    
end


function Reset(self)
    self.commandInProgress = nil
    return self
end