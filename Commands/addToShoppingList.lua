function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Adds an item to the shopping list",
        parameterDefs = {
            { 
                name = "unitName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "shoppingList",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "priority",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "lineID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "count",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

function max(a, b)
    if a >= b then
        return a
    else
        return b
    end
end

function Run(self, units, parameter)
    
    local unitName = parameter.unitName
    local shoppingList = parameter.shoppingList
    local priority = parameter.priority
    local lineID = parameter.lineID
    local count = parameter.count

    for i, shoppingListItem in pairs(shoppingList) do
        if shoppingListItem.unitName == unitName and shoppingListItem.priority == priority and shoppingListItem.lineID == lineID then
            -- This item is already present in the shopping list
            -- if shoppingList[i].count == nil then
            --     shoppingList[i].count = count
            -- else
            --     shoppingList[i].count = max(shoppingList[i].count, count)
            -- end
            if shoppingListItem.count == nil then
                shoppingListItem.count = count
            else
                shoppingListItem.count = max(shoppingListItem.count, count)
            end
            return FAILURE
        end
    end

    shoppingList[#shoppingList + 1] = {
        unitName = unitName,
        priority = priority,
        lineID = lineID,
        count = count
    }

    return SUCCESS
end


function Reset(self)
    return self
end