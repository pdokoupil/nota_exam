local sensorInfo = {
    name = "isInDanger",
    desc = "Returns true if the unit is in danger, false otherwise",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end


local SensorsGetUnitPosition = Sensors.getUnitPosition
local SensorsGetDPSRatioForArea = Sensors.getDPSRatioForArea

-- @description returns whether unit is in danger
return function(uid, radius, treshold)
    local unitLoc = SensorsGetUnitPosition(uid)
    
    if unitLoc == nil then
        return nil
    end

    local DPSRatio = SensorsGetDPSRatioForArea(unitLoc, radius, false, true)
    return DPSRatio < treshold
end