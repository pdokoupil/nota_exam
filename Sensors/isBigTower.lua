local sensorInfo = {
    name = "isBigTower",
    desc = "Returns true if the specified unit is big tower i.e. shika",
    author = "dok",
    date = "2020-07-15",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return whether the given unit is big tower or not
return function(unitName)
    return unitName == "shika"
end