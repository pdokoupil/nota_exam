local sensorInfo = {
    name = "removeCheapestShoppingItem",
    desc = "Remove and return cheapest item from the shopping list",
    author = "dok",
    date = "2020-07-09",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local SpringGetTeamUnits = Spring.GetTeamUnits
--local SpringGetMyTeamID = Spring.GetMyTeamID
--local SpringGetUnitDefID = Spring.GetUnitDefID
local SensorsCanBuyUnit = Sensors.canBuyUnit

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description removes cheapest item from the shopping list
return function(shoppingList, unitPrices)

    local cheapestPrice = 0
    local cheapestItemIndex = 0
    local maxPriority = 0
    local cheapestItem = nil

    if bb.lastBuy == nil then
        bb.lastBuy = {"armmav", "armmav", "armmav"} -- start buying artilery (for each line)
    end

    for itemIndex, shoppingItem in pairs(shoppingList) do
        if shoppingItem.unitName ~= bb.lastBuy[shoppingItem.lineID] or bb.haveEnoughArtilery[shoppingItem.lineID] == true then
            if shoppingItem.priority > maxPriority then
                maxPriority = shoppingItem.priority
            end
        end
    end

    for itemIndex, shoppingItem in pairs(shoppingList) do
        if shoppingItem.unitName ~= bb.lastBuy[shoppingItem.lineID] or bb.haveEnoughArtilery[shoppingItem.lineID] == true then
            local price = unitPrices[shoppingItem.unitName] * shoppingItem.count
            local weightedPrice = price / (shoppingItem.priority / maxPriority)
            if weightedPrice < cheapestPrice or cheapestItemIndex == 0 then
                cheapestItemIndex = itemIndex
                cheapestPrice = weightedPrice
            end
        end
    end

    if cheapestItemIndex > 0 then
        cheapestItem = shoppingList[cheapestItemIndex]
        if not (SensorsCanBuyUnit(unitPrices, cheapestItem.unitName, cheapestItem.count)) then
            return nil
        end
        shoppingList[cheapestItemIndex] = nil -- remove from the shopping list

        if cheapestItem.unitName == "armmart" then
            bb.lastBuy[cheapestItem.lineID] = "armmart"
        elseif cheapestItem.unitName == "armmav" then
            bb.lastBuy[cheapestItem.lineID] = "armmav"
        end
    end

    return cheapestItem
end