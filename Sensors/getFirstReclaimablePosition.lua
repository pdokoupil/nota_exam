local sensorInfo = {
    name = "getFirstReclaimablePosition",
    desc = "Returns first position on the given line where is some reclaimable material",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SensorsGetAmountOfReclaimableMetalFromFeatures = Sensors.getAmountOfReclaimableMetalFromFeatures
local SpringGetFeaturesInSphere = Spring.GetFeaturesInSphere

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return first reclaimable position
return function(examLineInfo)
    
    local points = examLineInfo.points
    local radius = 600

    --for i=1, #points do
    for i=7, #points do -- start from our last tower position to speedup
        local pos = points[i].position
        local features = SpringGetFeaturesInSphere(pos.x, pos.y, pos.z, radius)
        local amountOfMetal = SensorsGetAmountOfReclaimableMetalFromFeatures(features)
        if amountOfMetal > 0 then
            return pos
        end
    end

end