local sensorInfo = {
    name = "isGroupInDanger",
    desc = "Returns true if the group is in danger, false otherwise",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end


local SensorsIsInDanger = Sensors.isInDanger

function min(a, b)
    if a <= b then
        return a
    else
        return b
    end
end

-- @description Returns true if the group is in danger, false otherwise
return function(units, radius, dpsTreshold, unitsInDangerRatioTreshold)

    if units == nil then
        return false
    end

    local unitsNB = min(#units, 5)
    local n = unitsNB
    local unitsInDangerNB = 0

    for i = 1, n do
        local inDangerResult = SensorsIsInDanger(units[i], radius, dpsTreshold)

        if inDangerResult == true then unitsInDangerNB = unitsInDangerNB + 1
        elseif inDangerResult == nil then unitsNB = unitsNB - 1 end

    end
    return (unitsInDangerNB / unitsNB) >= unitsInDangerRatioTreshold
end