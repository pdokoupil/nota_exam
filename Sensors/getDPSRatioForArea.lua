local sensorInfo = {
    name = "getDPSRatioForArea",
    desc = "Computes a ratio of our and enemy's DPS within the given area",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SPHERE_RADIUS = 1500

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SensorsIsEnemy = Sensors.isEnemy
local SensorsGetUnitDPS = Sensors.GetUnitDPS
local SensorsIsTower = Sensors.isTower
--local SensorsIsUnitAlive = Sensors.isUnitAlive
local SensorsIsBigTower = Sensors.isBigTower
local SensorsIsSmallTower = Sensors.isSmallTower
local SensorsIsTowerClosed = Sensors.isTowerClosed
local SensorsGetUnitName = Sensors.getUnitName

-- @description Computes a ratio of our and enemy's DPS within the given area
return function(position, radius, includeOurTowers, includeEnemyTowers)

    local unitsInSphere = SpringGetUnitsInSphere(position.x, position.y, position.z, radius)
    local myDPS = 0
    local enemyDPS = 0


    for i = 1, #unitsInSphere do
        local uid = unitsInSphere[i]
        local unitName = SensorsGetUnitName(uid)
        local uDPS = SensorsGetUnitDPS(uid)

        if SensorsIsEnemy(uid) then
            if includeEnemyTowers and (SensorsIsBigTower(unitName) or (SensorsIsSmallTower(unitName) and not(SensorsIsTowerClosed(uid)))) then
                enemyDPS = enemyDPS + 10000 -- WIPE
            else
                enemyDPS = enemyDPS + uDPS
            end
            
            -- if includeEnemyTowers or (not(SensorsIsTower(uid))) then
            --     enemyDPS = enemyDPS + uDPS
            -- end
        else
            if includeOurTowers or (not(SensorsIsTower(unitName))) then
                myDPS = myDPS + uDPS
            end
        end

    end

    if enemyDPS == 0 then
        return 100000 -- this position is safe for sure
    end
    return myDPS / enemyDPS
end