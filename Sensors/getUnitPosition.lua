local sensorInfo = {
    name = "getUnitPosition",
    desc = "Returns unit position",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end


-- @description Returns unit position
return function(unitId)
    
    if unitId == nil then
        return nil
    end
    
    return Vec3(SpringGetUnitPosition(unitId))
end