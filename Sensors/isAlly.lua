local sensorInfo = {
    name = "isAlly",
    desc = "Returns true if the specified unit is from our ally, false otherwise",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local SpringGetMyTeamID = Spring.GetMyTeamID
--local SpringGetTeamInfo = Spring.GetTeamInfo
local SpringGetUnitAllyTeam = Spring.GetUnitAllyTeam
local SpringGetMyAllyTeamID = Spring.GetMyAllyTeamID

local myAllyTeamId = SpringGetMyAllyTeamID()

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return whether the given unit is Ally or not
return function(unitId)
    
    -- local myTeamId = SpringGetMyTeamID()
    -- local _, _, _, _, _, allyTeam, _, _ = SpringGetTeamInfo(myTeamId) -- team ID, leader, is dead, is AI team, side, ally team, custom team keys, income multiplier
    -- return allyTeam == SpringGetUnitAllyTeam(unitId)
    return myAllyTeamId == SpringGetUnitAllyTeam(unitId)
end