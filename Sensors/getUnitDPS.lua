local sensorInfo = {
    name = "getUnitDPS",
    desc = "Returns DPS of the unit",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitDefID = Spring.GetUnitDefID

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local dpsPerUnitDefId = {}
local UNKNOWN_DPS = 50

-- @description return DPS of the unit
return function(unit)
    if unit == nil then return 0 end

    local unitDefId = SpringGetUnitDefID(unit)
    if not unitDefId then 
        return UNKNOWN_DPS
    end

    -- Cache
    if dpsPerUnitDefId[unitDefId] ~= nil then
        return dpsPerUnitDefId[unitDefId]
    end

    -- If no weapons, return 0
    local unitDef = UnitDefs[unitDefId]
    if not unitDef.weapons or #unitDef.weapons < 1 then 
        dpsPerUnitDefId[unitDefId] = 0
        return dpsPerUnitDefId[unitDefId]
    end

    -- If no weapon def, also return 0
    local weaponDefId = unitDef.weapons[1].weaponDef 
    if not weaponDefId then 
        dpsPerUnitDefId[unitDefId] = 0
        return dpsPerUnitDefId[unitDefId]
    end

    local weapon = WeaponDefs[weaponDefId]
    if not weapon then 
        dpsPerUnitDefId[unitDefId] = UNKNOWN_DPS
        return dpsPerUnitDefId[unitDefId]
    end

    dpsPerUnitDefId[unitDefId] = weapon.damages[0] / weapon.reload

    return dpsPerUnitDefId[unitDefId]
end