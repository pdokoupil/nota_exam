local sensorInfo = {
    name = "tableIsEmpty",
    desc = "Returns true the table is empty",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description
return function(table)
    
    if table == nil then
        return true
    end

    for k, v in pairs(table) do
        return false
    end

    return true
    
end