local sensorInfo = {
    name = "filterAliveUnits",
    desc = "Takes array of unitIDs and return array of unitIDs of alive units",
    author = "dok",
    date = "2020-07-09",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SensorsIsUnitAlive = Sensors.isUnitAlive

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description filters alive units
return function(units)
    local aliveUnits = {}

    for key, value in pairs(units) do
        for i=1, #value do
            local unitId = value[i]
            if SensorsIsUnitAlive(unitId) then
                if aliveUnits[key] == nil then
                    aliveUnits[key] = {}
                end
                aliveUnits[key][#aliveUnits[key] + 1] = unitId
            end
        end
    end

    return aliveUnits
end