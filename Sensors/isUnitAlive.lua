local sensorInfo = {
    name = "isUnitAlive",
    desc = "Return true if the unit is alive, false otherwise",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitHealth = Spring.GetUnitHealth

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return whether the unit is alive or not
return function(unitId)
    
    return SpringGetUnitHealth(unitId) ~= nil

end