local sensorInfo = {
    name = "getTableSize",
    desc = "Returns table size",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description
return function(table)
    
    if table == nil then
        return 0
    end

    local c = 0
    for k, v in pairs(table) do
        c = c + 1
    end

    return c
    
end