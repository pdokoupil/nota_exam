local sensorInfo = {
    name = "getSafePosition",
    desc = "Return safe position for the units to 'escape' from dangerous position",
    author = "dok",
    date = "2020-07-09",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitDefID = Spring.GetUnitDefID

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return safe position on the given line
return function(lineInfo, examLineInfo)
    
    local battleIndex = lineInfo.battleIndex

    if battleIndex - 3 >= 1 then
        return examLineInfo.points[battleIndex - 3].position
    else
        -- TODO
        return 1 --positionOfOurBase
    end    
end