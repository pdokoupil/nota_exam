local sensorInfo = {
    name = "getUnitDPS",
    desc = "Return enemy unit with highest DPS in a given sphere",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SensorsIsEnemy = Sensors.isEnemy
local SensorsGetUnitDPS = Sensors.getUnitDPS
local SensorsIsUnitAlive = Sensors.isUnitAlive

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return the enemy with highest DPS in a given sphere
return function(sphereCenter, radius)

    local sphereUnits = SpringGetUnitsInSphere(sphereCenter.x, sphereCenter.y, sphereCenter.z, radius)
    local bestDPSUnit = nil
    local bestDPS = 0

    for i=1, #sphereUnits do
        local unitId = sphereUnits[i]
        if SensorsIsEnemy(unitId) and SensorsIsUnitAlive(unitId) then
            local currDPS = SensorsGetUnitDPS(unitId)
            if currDPS > bestDPS then
                bestDPS = currDPS
                bestDPSUnit = unitId
            end
        end
    end

    return bestDPSUnit
end