local sensorInfo = {
    name = "getUnitName",
    desc = "Translates unit ID to its name",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitDefID = Spring.GetUnitDefID

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end


-- @description
return function(unitId)
    
    local unitDef = SpringGetUnitDefID(unitId)
    return UnitDefs[unitDef].name
    
end