local sensorInfo = {
    name = "unitsAreInFavorablePositions",
    desc = "Returns true if the units are in a favorable position (favorable for our team) false otherwise",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SensorsCheckOurUnitIsInFavorablePosition = Sensors.checkOurUnitIsInFavorablePosition

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description determine whether the unit is in favorable position
return function(units)

    if units == nil then
        return tru
    end
    
    local result = true
    for i=1, #units do
        local unitId = units[i]
        result = result and SensorsCheckOurUnitIsInFavorablePosition(unitId)
        if not (result) then
            return false
        end

    end

    return result

end