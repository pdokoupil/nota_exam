local sensorInfo = {
    name = "isTowerClosed",
    desc = "Returns true if the specified ALIVE SMALL tower is closed",
    author = "dok",
    date = "2020-07-15",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitIsActive = Spring.GetUnitIsActive

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return whether the given small tower is closed
return function(unitId)
    return SpringGetUnitIsActive(unitId) ~= true
end