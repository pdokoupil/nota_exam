local sensorInfo = {
    name = "canBuyUnit",
    desc = "Returns true if we can buy the unit, false otherwise",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetTeamResources = Spring.GetTeamResources

local myTeamId = SpringGetMyTeamID()

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return whether we can buy the unit or not
return function(unitPrices, unitName, numberOfUnits)
    
    local metalAvailable = SpringGetTeamResources(myTeamId, "metal")
    return metalAvailable >= unitPrices[unitName] * numberOfUnits
    
end