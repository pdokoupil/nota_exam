local sensorInfo = {
    name = "getAmountOfReclaimableMetalFromFeatures",
    desc = "Returns the amount of metal that is reclaimable from the features",
    author = "dok",
    date = "2020-07-07",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetFeatureResources = Spring.GetFeatureResources

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return the amount of metal reclaimable from the features
return function(features)
    
    local reclaimableAmount = 0

    for i=1, #features do
        local rec = SpringGetFeatureResources(features[i])
        if rec > 0 then
            reclaimableAmount = reclaimableAmount + rec
        end
    end

    return reclaimableAmount
    
end