local sensorInfo = {
    name = "checkPositionIsFavorable",
    desc = "Returns true if the position is favorable",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition
local SensorsGetDpsInSphere = Sensors.GetDpsInSphere

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description determine whether the position is favorable
return function(position)

    if position == nil then
        return false
    end
    
    local radius = 500
    local favorableRatioThreshold = 1.0

    local dpsInSphere = SensorsGetDpsInSphere(position, radius)

    return (dpsInSphere.ourDPS / dpsInSphere.enemyDPS) > favorableRatioThreshold

end