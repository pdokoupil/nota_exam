local sensorInfo = {
    name = "getUnitsInfo",
    desc = "Return info about units, meaning their assignment to lines and a shoppingList",
    author = "dok",
    date = "2020-07-09",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitDefID = Spring.GetUnitDefID
local SensorsGetUnitName = Sensors.getUnitName

local teamID = SpringGetMyTeamID()

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return info about units (their assignment to lines and a shoppingList)
return function(unitsInfo, lines)
    
    if unitsInfo == nil then
        unitsInfo = {
            unitToLineAssignment = {},
            shoppingList = {}
        }
    end

    local currentAssignment = unitsInfo.unitToLineAssignment
    local currentShoppingList = unitsInfo.shoppingList

    local newAssignment = {} -- create new assignment for the units

    local ourUnits = SpringGetTeamUnits(teamID)

    for i=1, #ourUnits do
        -- Go over all the units
        local unitId = ourUnits[i]
        if currentAssignment[unitId] == nil then
            -- We find an entry in shoppingList with this unit
            local unitName = SensorsGetUnitName(unitId)

            local maxPriority = 0
            local maxPriorityItemIndex = 0

            -- The entry with a given unit and highest priority should be served
            for itemIndex, shoppingListEntry in pairs(currentShoppingList) do
                if shoppingListEntry.unitName == unitName and shoppingListEntry.priority > maxPriority then
                    maxPriority = shoppingListEntry.priority
                    maxPriorityItemIndex = itemIndex
                end
            end

            -- If we found the entry
            if maxPriorityItemIndex > 0 then
                -- We "buy" the item from shoppingList meaning that we remove the item from the shoppingList and assign the unit to the lineId which "ordered" the unit

                local  item = currentShoppingList[maxPriorityItemIndex]
                -- Delete the item from the shoppingList because it became satisfied
                currentShoppingList[maxPriorityItemIndex] = nil

                local unitsOnLine = lines[item.lineID].units
                
                if unitsOnLine == nil then
                    unitsOnLine = {}
                end

                if unitsOnLine[unitName] == nil then
                    unitsOnLine[unitName] = {}
                end

                unitsOnLine[unitName][#unitsOnLine[unitName] + 1] = unitId
                --unitsOnLine[unitName] = 54564564

                newAssignment[unitId] = item.lineID
            end

        else
            -- Otherwise the unit is already assigned so use that assignment
            newAssignment[unitId] = currentAssignment[unitId]
        end
    end

    -- Return updated version of unitsInfo
    return {
        unitToLineAssignment = newAssignment,
        shoppingList = currentShoppingList
    }

end