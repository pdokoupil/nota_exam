local sensorInfo = {
    name = "checkOurUnitIsInFavorablePosition",
    desc = "Returns true if the unit is in a favorable position (favorable for our team) false otherwise",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition
local SensorsGetDpsInSphere = Sensors.GetDpsInSphere

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description determine whether the unit is in favorable position
return function(unitId)
    
    local radius = 800
    local favorableRatioThreshold = 2.0

    local unitPosition = Vec3(SpringGetUnitPosition(unitId))
    local dpsInSphere = SensorsGetDpsInSphere(unitPosition, radius)

    return (dpsInSphere.ourDPS / dpsInSphere.enemyDPS) > favorableRatioThreshold

end