local sensorInfo = {
    name = "updateLineInfo",
    desc = "Takes current line info plus the info about line from exam mission's missionInfo (not available yet) and update line info (for example battleIndex)",
    author = "dok",
    date = "2020-07-09",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SensorsCoreEnemyTeamIDs = Sensors.core.EnemyTeamIDs
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetMyAllyTeamID = Spring.GetMyAllyTeamID

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description examLineInfo is info about line/corridor given from exam mission's missionInfo (not yet known), lineInfo is our current information about the line
return function(lineInfo, examLineInfo)
    local pointsOnLine = examLineInfo.points -- we expect similar interface

    local lineID = lineInfo.lineID
    local radius = 600

    local myAllyId = SpringGetMyAllyTeamID()
    local enemyTeams = SensorsCoreEnemyTeamIDs()
    local firstEnemyStrongpoint = nil
    local battleIndex = nil
    local foundBattleIndex = false

    --for i=1, #pointsOnLine do
    for i=7, #pointsOnLine do -- Optimization
        local pointOnLine = pointsOnLine[i]
        local point = pointOnLine.position
        local numOfEnemyUnits = 0

        if pointOnLine.isStrongpoint == true and pointOnLine.ownerAllyID ~= myAllyId and firstEnemyStrongpoint == nil then
            firstEnemyStrongpoint = point
        end
    
        for j=1, #enemyTeams do
            local unitsInSphere = #SpringGetUnitsInSphere(point.x, point.y, point.z, radius, enemyTeams[j])
            numOfEnemyUnits = numOfEnemyUnits + unitsInSphere
            if numOfEnemyUnits > 0 then
                break
            end
        end

        if foundBattleIndex == false then
            if numOfEnemyUnits > 0 then
                battleIndex = i -- index of the first point on the line (counted from our side) where the battle is happening
                foundBattleIndex = true
            else
                if lineInfo ~= nil and lineInfo.battleIndex ~= nil then
                    battleIndex = lineInfo.battleIndex
                else
                    --Use some default value .. TODO based on the MAP !
                    battleIndex = 8 -- in the beginning, the battle is in the middle of the line
                end
            end
        end
    end

    if firstEnemyStrongpoint == nil then
        firstEnemyStrongpoint = pointsOnLine[#pointsOnLine].position
    end

    return {
        battleIndex = battleIndex,
        lineID = lineID,
        firstEnemyStrongpoint = firstEnemyStrongpoint
    }

end