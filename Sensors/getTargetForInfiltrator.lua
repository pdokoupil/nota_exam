local sensorInfo = {
    name = "getTargetForInfiltrator",
    desc = "Return target for infiltrator",
    author = "dok",
    date = "2020-07-14",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
--local SpringGetMyAllyTeamID = Spring.GetMyAllyTeamID
--local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SensorsIsUnitAlive = Sensors.isUnitAlive
local SensorsIsEnemy = Sensors.isEnemy
local SensorsGetUnitName = Sensors.getUnitName
local SensorsIsBigTower = Sensors.isBigTower
local SensorsIsSmallTower = Sensors.isSmallTower
local SensorsGetUnitPosition = Sensors.getUnitPosition

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

function min(a, b)
    if a <= b then
        return a
    else
        return b
    end
end

-- function getUnitName(unitId)
--     local unitDefId = Spring.GetUnitDefID(unitId)
--     return UnitDefs[unitDefId].name
-- end

-- @description return target for an infiltrator unit
return function(firstEnemyStrongpoint)
    
    local point = firstEnemyStrongpoint
    local sideLen = 200

    local bigTowerPos = nil
    local smallTowerPos = {}

    local units = SpringGetUnitsInRectangle(point.x - 200, point.z - 200, point.x + 200, point.z + 200)
    for j=1, #units do
        local unitId = units[j]
        
        if SensorsIsUnitAlive(unitId) and SensorsIsEnemy(unitId) then
            --local unitName = getUnitName(unitId)
            local unitName = SensorsGetUnitName(unitId)
            if SensorsIsBigTower(unitName) then
                bigTowerPos = SensorsGetUnitPosition(unitId)
            elseif SensorsIsSmallTower(unitName) then
                smallTowerPos[#smallTowerPos + 1] = SensorsGetUnitPosition(unitId)
            end
        end
    end

    if bigTowerPos == nil then
        local smallTowerPosSize = #smallTowerPos

        if smallTowerPosSize == 0 then
            return firstEnemyStrongpoint
        elseif smallTowerPosSize == 1 then
            return smallTowerPos[1] + Vec3(-200, 0, -200)
        elseif smallTowerPosSize == 2 then
            if smallTowerPos[1]:Distance(smallTowerPos[2]) >= 300 then
                return (smallTowerPos[1] + smallTowerPos[2]) / 2
            else
                return smallTowerPos[1] + Vec3(-200, 0, -200)
            end
        else
            Spring.Echo("THIS SHOULD NEVER HAPPEN")
        end
    end

    local possibleInfiltratorPositions = {
        Vec3(bigTowerPos.x - 200, bigTowerPos.y, bigTowerPos.z - 200),
        Vec3(bigTowerPos.x, bigTowerPos.y, bigTowerPos.z - 200),
        Vec3(bigTowerPos.x + 200, bigTowerPos.y, bigTowerPos.z - 200)
        --Vec3(bigTowerPos.x - 200, bigTowerPos.y, bigTowerPos.z - 200)
    }

    local furthestAway = nil
    local furthestAwayDist = 0

    -- Iterate over all posible infiltrator positions
    for i=1, #possibleInfiltratorPositions do
        local currPos = possibleInfiltratorPositions[i]
        local minDist = 10000
        -- For each possible position, find a minimum distance to small tower
        for j=1, #smallTowerPos do
            local smallPos = smallTowerPos[j]
            local currDist = currPos:Distance(smallPos)
            minDist = min(minDist, currDist)
        end

        -- Choose the possible position if maximal minimum distance to small tower
        if minDist > furthestAwayDist then
            furthestAway = currPos
        end
    end

    return furthestAway
end