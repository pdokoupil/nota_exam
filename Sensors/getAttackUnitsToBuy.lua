local sensorInfo = {
    name = "getAttackUnitsToBuy",
    desc = "Returns a name of unit which should be bought",
    author = "dok",
    date = "2020-07-10",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SensorTableIsEmpty = Sensors.tableIsEmpty

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return name of a unit that should be bought
return function(line)
    
    if bb.lastEnqueue == nil then
        bb.lastEnqueue = {"armmav", "armmav", "armmav"} -- to start with buying artilery
    end

    if bb.haveEnoughArtilery == nil then
        bb.haveEnoughArtilery = {}
    end

    local availableUnitsOnLine = line.units
    local lineID = line.lineInfo.lineID

    if SensorTableIsEmpty(availableUnitsOnLine) then
        availableUnitsOnLine["armmart"] = {}
        availableUnitsOnLine["armmav"] = {}
    end

    if availableUnitsOnLine["armmart"] ~= nil and #availableUnitsOnLine["armmart"] >= 10 then
        bb.lastEnqueue[lineID] = "armmart" -- force buying mavericks
        bb.haveEnoughArtilery[lineID] = true
    else
        bb.haveEnoughArtilery[lineID] = false
    end

    if bb.lastEnqueue[lineID] == "armmart" then
        -- lastly we bought armmart so now we buy 
        bb.lastEnqueue[lineID] = "armmav"
        return {
            unitName = "armmav",
            count = 4
        }
    elseif bb.lastEnqueue[lineID] == "armmav" then
        -- vice versa
        bb.lastEnqueue[lineID] = "armmart"
        return {
            unitName = "armmart",
            count = 2
        }
    end    
end