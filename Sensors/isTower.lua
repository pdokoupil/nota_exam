local sensorInfo = {
    name = "isTower",
    desc = "Returns true if the specified unit is some kind of 'tower'",
    author = "dok",
    date = "2020-07-13",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return whether the given unit is tower or not
return function(unitName)
    return unitName == "shika" or unitName == "corvipe" or unitName == "armllt"
end