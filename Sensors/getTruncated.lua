local sensorInfo = {
    name = "getTruncated",
    desc = "",
    author = "dok",
    date = "2020-07-13",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return safe position on the given line
return function(array, index)
    
    if index < 1 then
        return array[1]
    elseif index > #array then
        return array[#array]
    else
        return array[index]
    end
    
end