local sensorInfo = {
    name = "getTeamResource",
    desc = "Return the amount of a given resource for a given team",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetTeamResources = Spring.GetTeamResources

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description Return the amount of a given resource for a given team
return function(teamId, resourceName)
    
    return SpringGetTeamResources(teamId, resourceName)

end