local sensorInfo = {
    name = "getTargetForArtilery",
    desc = "Return enemy for artilery",
    author = "dok",
    date = "2020-07-08",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
-- local SpringGetMyAllyTeamID = Spring.GetMyAllyTeamID
-- local SpringGetUnitDefID = Spring.GetUnitDefID
local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SensorsIsTower = Sensors.isTower
local SensorsIsUnitAlive = Sensors.isUnitAlive
local SensorsIsBigTower = Sensors.isBigTower
local SensorsIsSmallTower = Sensors.isSmallTower
local SensorsGetUnitName = Sensors.getUnitName

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description return the enemy target for the artilery
return function(firstEnemyStrongpoint)
    
    -- local myAllyId = SpringGetMyAllyTeamID()
    
    -- for i=1, #linePoints do
    --     local point = linePoints[i].position
    --     if linePoints[i].isStrongpoint == true and linePoints[i].ownerAllyID ~= myAllyId then
    --         -- Spring.Echo("There should be point.y") TODO!!!!!!
    --         local units = SpringGetUnitsInRectangle(point.x - 200, point.z - 200, point.x + 200, point.z + 200)
    --         for j=1, #units do
    --             local unitId = units[j]
    --             if SensorsIsTower(unitId) and SensorsIsUnitAlive(unitId) then
    --                 Spring.Echo("Returning " .. unitId .. " as target for artilery")
    --                 return unitId
    --             end
    --         end
    --     end
    -- end

    if firstEnemyStrongpoint.x == nil or firstEnemyStrongpoint.y == nil or firstEnemyStrongpoint.z == nil then
        return nil
    end

    local units = SpringGetUnitsInRectangle(firstEnemyStrongpoint.x - 200, firstEnemyStrongpoint.z - 200, firstEnemyStrongpoint.x + 200, firstEnemyStrongpoint.z + 200)
    local smallTower = nil

    for j=1, #units do
        local unitId = units[j]
        local unitName = SensorsGetUnitName(unitId)
        if SensorsIsUnitAlive(unitId) then
            if SensorsIsBigTower(unitName) then
                return unitId
            elseif SensorsIsSmallTower(unitName) then
                smallTower = unitId
            end
        end
    end

    return smallTower
end