local sensorInfo = {
    name = "ensureDisabledFireState",
    desc = "Ensured that the given units have disabled firestate",
    author = "dok",
    date = "2020-07-13",
    license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local param = {0}
local modifier = {}

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT 
    }
end

-- @description disable fire state for the given units
return function(artileryUnits)

    if artileryUnits == nil then
        return nil
    end

    if bb.artileryDisabled == nil then
        bb.artileryDisabled = {}
    end

    for i=1, #artileryUnits do
        local unitId = artileryUnits[i]
        if bb.artileryDisabled[unitId] == nil then
            SpringGiveOrderToUnit(unitId, CMD.FIRE_STATE, param, modifier)
            bb.artileryDisabled[unitId] = true
        end
    end
end